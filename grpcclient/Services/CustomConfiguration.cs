using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace GRpcClient.Services
{
    public class CustomConfiguration
    {
        private readonly ILogger<CustomConfiguration> _logger;
        private readonly IConfiguration _configuration;

        public CustomConfiguration(ILoggerFactory logger, IConfiguration configuration)
        {
            _logger = logger.CreateLogger<CustomConfiguration>();
            _configuration = configuration;
        }

        public async ValueTask<string> GetConfigNameAsync() 
        {
            return await ValueTask.FromResult("NewConfiguration");
        }
    }

}