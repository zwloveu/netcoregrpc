using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace GRpcServer
{
    public class GreeterService : Greet.Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
        }

        public override Task<Greet.HelloReply> SayHello(Greet.HelloRequest request, ServerCallContext context)
        {
            return Task.FromResult(new Greet.HelloReply
            {
                Message = "Hello " + request.Name
            });
        }
    }
}
