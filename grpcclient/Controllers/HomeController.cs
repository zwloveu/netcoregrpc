﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using GRpcClient.Models;
using GRpcClient.Services;

namespace GRpcClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly CustomConfiguration _cc;
        private readonly Greet.Greeter.GreeterClient _greeterClient;

        public HomeController(ILogger<HomeController> logger, CustomConfiguration cc,
            Greet.Greeter.GreeterClient greeterClient)
        {
            _logger = logger;
            _cc = cc;
            _greeterClient = greeterClient;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["NEW_CC"] = await _cc.GetConfigNameAsync();
            var request = new Greet.HelloRequest() { Name = ViewData["NEW_CC"].ToString() };
            ViewData["GRMsg"] = await _greeterClient.SayHelloAsync(request);
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
