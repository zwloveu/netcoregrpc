# GRPC DotnetCore SERVER/CLIENT

## Server Packages
## Install required packages
```
dotnet add package Grpc.AspNetCore
```

## Client
### Install required packages
```
dotnet add package Grpc.Net.Client
dotnet add package Google.Protobuf
dotnet add package Grpc.Tools
dotnet add package Grpc.Net.ClientFactory

```
### Add your proto files to client in the csproj file
```
  <ItemGroup>
    <Protobuf Include="Protos\greet.proto" GrpcServices="Client" />
  </ItemGroup>
```

## Local Certs
```
$ openssl genrsa -out private_ids.key 2048
$ openssl req -new -x509 -key private_ids.key -days 3650 -out public_ids.crt
$ openssl pkcs12 -export -in public_ids.crt -inkey private_ids.key -out ids.pfx
```